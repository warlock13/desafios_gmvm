

from flask import Flask, jsonify
import os  # Importa el módulo 'os' para acceder a las variables de entorno
import boto3
import csv

# Crear una instancia de Flask
app = Flask(__name__)

# Configurar las credenciales de AWS desde variables de entorno
os.environ["AWS_ACCESS_KEY_ID"] = "AKIAWGPVQ7CPOM5WA27I"
os.environ["AWS_SECRET_ACCESS_KEY"] = "hcHChpgvQiOnn4xT1OYsUdVOZeJISzmYefInyRZq"



# Nombre del bucket y archivo CSV
bucket_name = 'ue2-desafios'
csv_file_key = 'datos_empleados_faker.csv'

# Configurar el cliente de S3 con las credenciales
s3 = boto3.client('s3')

# Definir una ruta para obtener los datos del archivo CSV
@app.route('/data', methods=['GET'])
def get_data():
    try:
        # Descargar el archivo CSV desde S3
        response = s3.get_object(Bucket=bucket_name, Key=csv_file_key)
        csv_data = response['Body'].read().decode('utf-8')
        
        # Parsear el CSV
        data = []
        reader = csv.DictReader(csv_data.splitlines())
        for row in reader:
            data.append(row)

        # Devolver los datos en formato JSON
        return jsonify(data)
    except Exception as e:
        return str(e), 500

# Ejecutar la aplicación Flask
if __name__ == '__main__':
    app.run(debug=True)


